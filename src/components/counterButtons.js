import React, {Component} from 'react';
import { Container , ButtonIncrement, ButtonDecrement, ButtonReset } from '../styles/styles';

class Counter extends Component {
  render() {
    return (
      <Container>
        <ButtonIncrement onClick={this.props.increment}>Adicionar</ButtonIncrement>
        <ButtonDecrement onClick={this.props.decrement}>Remover</ButtonDecrement>
        <ButtonReset onClick={this.props.reset}>Resetar</ButtonReset>
      </Container>
    );
  }
}

export default Counter;