import React, {Component} from 'react';
import CounterButtons from './counterButtons';
import { Container, Subtitle } from '../styles/styles';

class Counter extends Component {

  constructor (props) {
    super(props);
    this.state = {
      count: 0
    }
  }

  increment = () => {
    this.setState({
      count: this.state.count + 1
    });
  }
  
  decrement = () => {
    this.setState({
      count: this.state.count - 1
    });
  }

  reset = () => {
    this.setState({
      count: 0
    });
  }

  render() {
    return (
      <Container>
        <Subtitle>Valor: {this.state.count} </Subtitle>
        <CounterButtons increment={this.increment} decrement={this.decrement} reset={this.reset} />
      </Container>
    );
  }
}

export default Counter;