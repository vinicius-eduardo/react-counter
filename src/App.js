import React from 'react';
import Counter from './components/counter';
import GlobalStyle, { Container, Title } from './styles/styles';
import logAcens from './assets/RedondaAzul.ico';

function App() {
  return (
      <Container>
        <GlobalStyle />
        <img src={logAcens} alt='Logo Acens' width='90px' />
        <Title>ContAcens, o contador da Acens!</Title>
        <Counter />
      </Container>
  );
}

export default App;
