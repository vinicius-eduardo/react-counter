import styled, { createGlobalStyle } from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.h1`
  margin: 1rem 0;
`;

export const Subtitle = styled.h2`
  margin-bottom: 1rem;
`;

export const ButtonIncrement = styled.button`
  background: transparent;
  cursor: pointer;
  width: 10rem;
  border-radius: 3px;
  border: 2px solid blueviolet;
  color: blueviolet;
  margin: .5rem;
  padding: 0.5rem 1rem;
  transition: all .1s ease-in-out;

  &:hover {
    transform: scale(1.1);
  }
`;

export const ButtonDecrement = styled.button`
  background: transparent;
  cursor: pointer;
  width: 10rem;
  border-radius: 3px;
  border: 2px solid #a41111;
  color: #a41111;
  margin: .5rem;
  padding: 0.5rem 1rem;
  transition: all .1s ease-in-out;
  
  &:hover {
    transform: scale(1.1);
  }
`;

export const ButtonReset = styled.button`
  background: red;
  cursor: pointer;
  width: 10rem;
  border-radius: 3px;
  border: 2px solid red;
  color: white;
  margin: .5rem;
  padding: 0.5rem 1rem;
  transition: all .1s ease-in-out;

  &:hover {
    transform: scale(1.1);
  }
`;

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  body {
    font-family: arial;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: aqua;
  }
`;